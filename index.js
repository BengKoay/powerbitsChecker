const csv = require('fast-csv');
const fs = require('fs');
const axios = require('axios');
const moment = require('moment');

const ObjectsToCsv = require('objects-to-csv');
const directory = 'output';
const writeToCSV = [];
const axiosSolar = {
	baseURL: 'https://5l68pc71bd.execute-api.ap-southeast-1.amazonaws.com/',
	authorization: 'a414f268-5ab2-4854-b4f7-97a9ea6d6fec',
};
const axiosMainHelper = axios.create({
	baseURL: axiosSolar.baseURL,
	headers: { Authorization: axiosSolar.authorization },
});

// const startDate = moment('2022-06-22 00:00', 'YYYY-MM-DD HH:mm').unix();
// const dateNow = moment('2022-06-30 23:59', 'YYYY-MM-DD HH:mm').unix();
const startDate = moment().startOf('day').subtract(7, 'days').unix();
const dateNow = moment().unix();
// const allUUID = [];

const main = async () => {
	await fs
		.createReadStream('test.csv')
		.pipe(csv.parse({ headers: true }))
		.on('data', async (row) => {
			if (!!row && !!row.UUID) {
				// scan each UUID
				console.log(row.UUID);
				try {
					await readEachRow(row.UUID);
				} catch (e) {
					console.log('readEachRow', e);
				}
			}
		})
		.on('end', (rowCount) => console.log(`Parsed ${rowCount} rows`));
};

main();

const readEachRow = async (eachDevice) => {
	let dataOK = false;
	const dataAfterCheck = [];
	const errorFilename = [];
	// error code
	let currentDifference = false;
	let powerDifference = false;
	let powerProblem = false;
	let negativePower = false;
	let exception = false;
	let voltageError = false;
	let currentError = false;
	let energyError = false;
	let noPower = false;
	let gotLatestData = false;
	let currentDiffMoreThan50 = false;

	console.log('readEachRow');
	// for (const eachDevice of allUUID) {
	// let eachDevice = row.UUID;
	const res = await axiosMainHelper.get(
		// `devicePayload?deviceId=${eachDevice.uuid}&ref=${eachDevice.ref}&timestampFrom=${startDate}&timestampTo=${dateNow}`
		// `devicePayload?deviceId=${eachDevice.uuid}&ref=${eachDevice.ref}&timestampFrom=${startDate}&timestampTo=${dateNow}`
		`devicePayload?deviceId=${eachDevice}&ref=0&timestampFrom=${startDate}&timestampTo=${dateNow}`
		// `devicePayload?deviceId=${eachDevice}&ref=0`
	);
	// console.log(res.data.data[0].deviceId);
	// for (const eachData of res.data.data) {
	//     console.log(eachData)
	// }
	const allData = res.data.data;
	for (const eachData in allData) {
		timestamp = moment(allData[eachData].timestamp).format('YYYY-MM-DD HH:mm');
		allData[eachData].timestamp = moment(allData[eachData].timestamp).format(
			'YYYY-MM-DD HH:mm'
		);
		activeFwdEnergy = allData[eachData].activeFwdEnergy;
		totalActivePower = allData[eachData].totalActivePower;
		voltage1 = allData[eachData].voltage1;
		voltage2 = allData[eachData].voltage2;
		voltage3 = allData[eachData].voltage3;
		current1 = allData[eachData].current1;
		current2 = allData[eachData].current2;
		current3 = allData[eachData].current3;
		powerFactor = allData[eachData].powerFactor;
		frequency = allData[eachData].frequency;
		allData[eachData].averageCurrent = null;
		allData[eachData].errorDiffCurrent2 = null;
		allData[eachData].errorDiffCurrent3 = null;
		allData[eachData].errorDiffPower = null;
		allData[eachData].errorDiffCurrent3 = null;
		allData[eachData].calculatedApparentPower = null;
		allData[eachData].calculatedActivePower = null;
		allData[eachData].errorDiffPower = null;
		allData[eachData].energyError = null;
		allData[eachData].errorVoltage = null;
		allData[eachData].errorCurrent = null;
		allData[eachData].gotLatestData = null;
		allData[eachData].currentDiffMoreThan30 = null;

		// eachData.errorDiffCurrent3 = null;
		// console.log(eachData);
		// console.log(moment(eachData.timestamp).format('YYYY-MM-DD HH:mm'));

		// check voltage

		let gotVoltage1 = false;
		let gotVoltage2 = false;
		let gotVoltage3 = false;
		let readingVoltage1 = false;
		let readingVoltage2 = false;
		let readingVoltage3 = false;
		let gotCurrent1 = false;
		let gotCurrent2 = false;
		let gotCurrent3 = false;
		let readingCurrent1 = false;
		let readingCurrent2 = false;
		let readingCurrent3 = false;
		// console.log(
		// 	moment(timestamp).format('YYYY-MM-DD HH:mm'),
		// 	moment.unix(dateNow).format('YYYY-MM-DD HH:mm'),
		// 	// moment(timestamp).diff(moment(dateNow)),
		// 	// moment.duration(moment(timestamp).diff(moment(dateNow)))
		// 	moment.duration(moment(timestamp).diff(moment.unix(dateNow))).asMinutes()
		// );
		if (
			Math.abs(
				moment
					.duration(moment(timestamp).diff(moment.unix(dateNow)))
					.asMinutes()
			) < 15
		) {
			dataOK = true;
			gotLatestData = true;
		} else
			allData[eachData].gotLatestData = Math.abs(
				moment
					.duration(moment(timestamp).diff(moment.unix(dateNow)))
					.asMinutes()
			);
		if (!!voltage1) {
			gotVoltage1 = true;
			if (voltage1 >= 200 && voltage1 <= 270) readingVoltage1 = true;
		}
		if (!!voltage2) {
			gotVoltage2 = true;
			if (voltage2 >= 200 && voltage2 <= 270) readingVoltage2 = true;
		}
		if (!!voltage3) {
			gotVoltage3 = true;
			if (voltage3 >= 200 && voltage3 <= 270) readingVoltage3 = true;
		}
		if (!!current1) {
			gotCurrent1 = true;
			if (current1 >= 0.2) readingCurrent1 = true;
		}
		if (!!current2) {
			gotCurrent2 = true;
			if (current2 >= 0.2) readingCurrent2 = true;
		}
		if (!!current3) {
			gotCurrent3 = true;
			if (current3 >= 0.2) readingCurrent3 = true;
		}

		if (readingVoltage1 && readingVoltage2 && readingVoltage3) {
			// 3 phase
			dataOK = true;
			if (readingCurrent1 && !readingCurrent2 && !readingCurrent1) {
				allData[eachData].errorCurrent = true;
				currentError = true;
			}
			if (!readingCurrent1 && readingCurrent2 && !readingCurrent1) {
				allData[eachData].errorCurrent = true;
				currentError = true;
			}
			if (!readingCurrent1 && !readingCurrent2 && readingCurrent1) {
				allData[eachData].errorCurrent = true;
				currentError = true;
			}
			if (!!current1 && !!current2 && !!current3) {
				// check current
				let averageCurrent = current1;
				let errorDiffCurrent2 =
					(Math.abs(current2 - averageCurrent) / averageCurrent) * 100;
				let errorDiffCurrent3 =
					(Math.abs(current3 - averageCurrent) / averageCurrent) * 100;
				allData[eachData].averageCurrent = averageCurrent;
				allData[eachData].errorDiffCurrent2 = errorDiffCurrent2;
				allData[eachData].errorDiffCurrent3 = errorDiffCurrent3;
				if (errorDiffCurrent2 >= 10 || errorDiffCurrent3 >= 10) {
					// console.log(
					// 	'current',
					// 	moment(timestamp).format('YYYY-MM-DD HH:mm'),

					// 	current1,
					// 	current2,
					// 	current3,
					// 	errorDiffCurrent2,
					// 	errorDiffCurrent2
					// );
					dataOK = false;
					currentDifference = true;
					if (errorDiffCurrent2 >= 50 || errorDiffCurrent3 >= 50) {
						currentDiffMoreThan50 = true;
						allData[eachData].currentDiffMoreThan50 =
							(errorDiffCurrent2 + errorDiffCurrent3) / 2;
					}
				}
			}
			if (
				!!totalActivePower &&
				!!powerFactor &&
				gotVoltage1 &&
				gotVoltage2 &&
				gotVoltage3
			) {
				// check power
				//   S = VI
				let calculatedApparentPower =
					(voltage1 * current1 + voltage2 * current2 + voltage3 * current3) /
					1000;
				let calculatedActivePower = calculatedApparentPower * powerFactor;
				let errorDiffPower =
					((calculatedActivePower - totalActivePower) / totalActivePower) * 100;
				allData[eachData].calculatedApparentPower = calculatedApparentPower;
				allData[eachData].calculatedActivePower = calculatedActivePower;
				if (errorDiffPower >= 1) {
					// if P = V * I * PF method differ 1%
					// console.log(
					// 	'totalActivePower',
					// 	calculatedActivePower,
					// 	totalActivePower,
					// 	errorDiffPower,
					// 	calculatedApparentPower,
					// 	powerFactor
					// );
					allData[eachData].errorDiffPower =
						'if P = V * I * PF method differ 1%';
					allData[eachData].errorDiffPower = errorDiffPower;
					powerDifference = true;
					dataOK = false;
				}
			} else {
				// console.log(
				// 	'power problem, no power or no power factor',
				// 	timestamp,
				// 	current1,
				// 	current2,
				// 	current3
				// );
				dataOK = false;
				noPower = true;
				allData[eachData].PowerProblem =
					'power problem, no power or no power factor';
				// errorFilename = 'PowerProblem';
			}
			if (totalActivePower < 0) {
				dataOK = false;
				negativePower = true;
				allData[eachData].PowerProblem = 'power problem,negative ';
			}

			// check energy in cumulative
			if (eachData != 0) {
				let energyDiff =
					allData[eachData].activeFwdEnergy -
					allData[eachData - 1].activeFwdEnergy;
				if (energyDiff > 2) {
					// check not 1st array
					dataOK = false;
					energyError = true;
					allData[eachData].energyError = energyDiff;
				}
			}
		} else if (readingVoltage1 && !readingVoltage2 && !readingVoltage3) {
			// 1 phase
			// if (voltage1 >= 220 && voltage1 <= 250);
			if (totalActivePower < 0) {
				dataOK = false;
				negativePower = true;
				allData[eachData].PowerProblem = 'power problem,negative ';
			}
			// else console.log(voltage1);
		} else if (readingVoltage1 && !readingVoltage2 && readingVoltage3) {
			dataOK = false;
			voltageError = true;
			allData[eachData].errorVoltage = true;
			// errorFilename = 'No voltage2';
			// console.log('No voltage2');
		} else if (readingVoltage1 && readingVoltage2 && !readingVoltage3) {
			dataOK = false;
			voltageError = true;
			allData[eachData].errorVoltage = true;
			// errorFilename = 'No voltage3';
			// console.log('No voltage3');
		} else if (!readingVoltage1 && readingVoltage2 && readingVoltage3) {
			dataOK = false;
			voltageError = true;
			allData[eachData].errorVoltage = true;

			// errorFilename = 'No voltage1';
			// console.log('No voltage1');
		} else {
			dataOK = false;
			exception = true;
			// errorFilename.push('Exception');
			// console.log(allData[eachData], 'Exception');
		}

		// ok
		//   console.log(eachData.voltage1);

		//   console.log()
		// eachData.comment = '';
		// writeToCSV.push(eachData);
		dataAfterCheck.push(allData[eachData]);
	}
	if (dataOK) {
		const csv = new ObjectsToCsv(dataAfterCheck);

		// Save to file:
		try {
			await csv.toDisk(`${directory}/${eachDevice}.csv`);
		} catch (e) {
			console.log('dataOK', e);
		}
	} else {
		if (!gotLatestData) errorFilename.push('noLatestData');
		if (energyError) errorFilename.push('energyError');
		if (negativePower) errorFilename.push('errorNegativePower');
		if (powerProblem) errorFilename.push('errorPowerProblem');
		if (powerDifference) errorFilename.push('errorDiffPower');
		if (currentDifference) {
			errorFilename.push('currentDifference');
			if (currentDiffMoreThan50) {
				errorFilename.push('50');
			}
		}
		if (exception) errorFilename.push('exception');
		if (voltageError) errorFilename.push('voltageError');
		if (noPower) errorFilename.push('noPower');
		if (currentError) errorFilename.push('currentError');

		const csv = new ObjectsToCsv(dataAfterCheck);

		// Save to file:
		try {
			await csv.toDisk(`${directory}/${errorFilename}_${eachDevice}.csv`);
		} catch (e) {
			console.log('not dataOK', e);
		}
	}
	// }

	// after check write csv
};
