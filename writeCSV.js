const axios = require('axios');
const moment = require('moment');

const ObjectsToCsv = require('objects-to-csv');

// const fs = require('fs');
// const fileName = 'randoms.csv';
// const csvFile = fs.createWriteStream(fileName);
const file = 'output.csv';

// const fastcsv = require('fast-csv');
// const fast_csv = fs.createWriteStream();
// const writeStream = fs.createWriteStream('outputfile.csv');
// fast_csv.pipe(writeStream);

const axiosSolar = {
	baseURL: 'https://5l68pc71bd.execute-api.ap-southeast-1.amazonaws.com/',
	authorization: 'a414f268-5ab2-4854-b4f7-97a9ea6d6fec',
};
const axiosMainHelper = axios.create({
	baseURL: axiosSolar.baseURL,
	headers: { Authorization: axiosSolar.authorization },
});

// const eachDevice = '003d431f-0d82-49e4-86b6-e0da86d199d7';
// const eachDevice = '049a6c40-3b10-4dc8-bb74-a29e7fa2c9cd'; // MSB 2 Puncak Seloka
const eachDevice = '0cd606d2-4115-4409-b225-c99ffc374749';
const startDate = moment('2022-06-28 00:00', 'YYYY-MM-DD HH:mm').unix();
const dateNow = moment('2022-06-28 23:59', 'YYYY-MM-DD HH:mm').unix();

const main = async () => {
	const res = await axiosMainHelper.get(
		// `devicePayload?deviceId=${eachDevice.uuid}&ref=${eachDevice.ref}&timestampFrom=${startDate}&timestampTo=${dateNow}`
		// `devicePayload?deviceId=${eachDevice.uuid}&ref=${eachDevice.ref}&timestampFrom=${startDate}&timestampTo=${dateNow}`
		`devicePayload?deviceId=${eachDevice}&ref=0&timestampFrom=${startDate}&timestampTo=${dateNow}`
		// `devicePayload?deviceId=${eachDevice}&ref=0`
	);
	// fast_csv.write(res.data.data); //each element inside bracket
	// fast_csv.end();

	const csv = new ObjectsToCsv(res.data.data);

	// Save to file:
	await csv.toDisk(`${file}`);
};

main();
