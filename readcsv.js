const fs = require('fs');
const csv = require('fast-csv');

fs.createReadStream('test.csv')
	.pipe(csv.parse({ headers: true }))
	.on('error', (error) => console.error(error))
	.on('data', (row) => console.log(row.UUID))
	.on('end', (rowCount) => console.log(`Parsed ${rowCount} rows`));
