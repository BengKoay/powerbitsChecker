const axios = require('axios');
const moment = require('moment');

const axiosSolar = {
	baseURL: 'https://5l68pc71bd.execute-api.ap-southeast-1.amazonaws.com/',
	authorization: 'a414f268-5ab2-4854-b4f7-97a9ea6d6fec',
};
const axiosMainHelper = axios.create({
	baseURL: axiosSolar.baseURL,
	headers: { Authorization: axiosSolar.authorization },
});

// const eachDevice = '003d431f-0d82-49e4-86b6-e0da86d199d7';
// const eachDevice = '049a6c40-3b10-4dc8-bb74-a29e7fa2c9cd'; // MSB 2 Puncak Seloka
const eachDevice = '0cd606d2-4115-4409-b225-c99ffc374749';
const startDate = moment('2022-06-28 00:00', 'YYYY-MM-DD HH:mm').unix();
const dateNow = moment('2022-06-28 23:59', 'YYYY-MM-DD HH:mm').unix();

const main = async () => {
	const res = await axiosMainHelper.get(
		// `devicePayload?deviceId=${eachDevice.uuid}&ref=${eachDevice.ref}&timestampFrom=${startDate}&timestampTo=${dateNow}`
		// `devicePayload?deviceId=${eachDevice.uuid}&ref=${eachDevice.ref}&timestampFrom=${startDate}&timestampTo=${dateNow}`
		`devicePayload?deviceId=${eachDevice}&ref=0&timestampFrom=${startDate}&timestampTo=${dateNow}`
		// `devicePayload?deviceId=${eachDevice}&ref=0`
	);
	// console.log(res.data.data);

	// loop res.data.data

	for (const eachData of res.data.data) {
		timestamp = moment(eachData.timestamp).format('YYYY-MM-DD HH:mm');
		activeFwdEnergy = eachData.activeFwdEnergy;
		totalActivePower = eachData.totalActivePower;
		voltage1 = eachData.voltage1;
		voltage2 = eachData.voltage2;
		voltage3 = eachData.voltage3;
		current1 = eachData.current1;
		current2 = eachData.current2;
		current3 = eachData.current3;
		powerFactor = eachData.powerFactor;
		frequency = eachData.frequency;
		// console.log(eachData);
		// console.log(moment(eachData.timestamp).format('YYYY-MM-DD HH:mm'));

		// check voltage

		if (!!voltage1 && !!voltage2 && !!voltage3) {
			// 3 phase

			if (!!voltage1) {
				// check voltage1
				if (voltage1 >= 220 && voltage1 <= 250);
				else console.log(voltage1);
			}

			if (!!voltage2) {
				// check voltage2
				if (voltage2 >= 220 && voltage2 <= 250);
				else console.log(voltage2);
			}
			if (!!voltage3) {
				// check voltage3
				if (voltage3 >= 220 && voltage3 <= 250);
				else console.log(voltage3);
			}

			if (!!current1 && !!current2 && !!current3) {
				// check current
				let averageCurrent = current1;
				let errorDiffCurrent2 =
					(Math.abs(current2 - averageCurrent) / averageCurrent) * 100;
				let errorDiffCurrent3 =
					(Math.abs(current3 - averageCurrent) / averageCurrent) * 100;
				if (errorDiffCurrent2 >= 10 || errorDiffCurrent3 >= 10)
					console.log(
						'current',
						moment(timestamp).format('YYYY-MM-DD HH:mm'),

						current1,
						current2,
						current3,
						errorDiffCurrent2,
						errorDiffCurrent2
					);
			}
			if (!!totalActivePower && !!powerFactor) {
				// check power
				//   S = VI
				let calculatedApparentPower =
					voltage1 * current1 + voltage2 * current2 + voltage3 * current3;
				let calculatedActivePower =
					(calculatedApparentPower / 1000) * powerFactor;
				let errorDiffPower =
					(Math.abs(calculatedActivePower - totalActivePower) /
						totalActivePower) *
					100;
				if (errorDiffPower >= 1)
					// if P = V * I * PF method differ 1%
					console.log(
						'totalActivePower',
						calculatedActivePower,
						totalActivePower,
						errorDiffPower,
						calculatedApparentPower,
						powerFactor
					);
			} else
				console.log(
					'power problem, no power or no power factor',
					timestamp,
					current1,
					current2,
					current3
				);
		} else if (!!voltage1 && !voltage2 && !voltage3) {
			// 1 phase
			if (voltage1 >= 220 && voltage1 <= 250);
			else console.log(voltage1);
		} else if (!!voltage1 && !voltage2 && !!voltage3) {
			console.log('No voltage2');
		} else if (!!voltage1 && !!voltage2 && !voltage3) {
			console.log('No voltage3');
		} else if (!voltage1 && !!voltage2 && !!voltage3) {
			console.log('No voltage1');
		} else {
			console.log(eachData);
		}

		// ok
		//   console.log(eachData.voltage1);

		//   console.log()
	}
};

main();
